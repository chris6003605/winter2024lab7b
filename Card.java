public class Card{
	private Suit suit;
	private Rank value;
	
	
	public Card(Suit suit, Rank value){
		this.suit = suit;
		this.value = value;
	}
	
	public Suit getSuit(){
		return this.suit;
	}
	public Rank getValue(){
		return this.value;
	}
	
	public double calculateScore(){
		return this.value.getScoreOfRank() + this.suit.getScoreOfSuit();
	}
		
	
	
	public String toString(){
		return this.value.name() + " of " + this.suit.name(); 
	}
}
