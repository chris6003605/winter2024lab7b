import java.util.Random;
public class Deck{
	private final int MAX_NUM_CARDS = 52;
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck()
	{
	
	//A array of string that holds value of all the card numbers
		
	this.numberOfCards = MAX_NUM_CARDS;
	this.cards = new Card[this.numberOfCards];	
	this.rng = new Random();
	
	int currentCard = 0;
	
	//for each suit (String) in the array SUITS
	for(Suit suit : Suit.values())
		{
		for(Rank rank : Rank.values())
			{
			this.cards[currentCard] = new Card(suit, rank);
			currentCard++;
			}
		}
	}
	public int length(){
		return numberOfCards;
	}
	public Card drawTopCard(){
		Card c = cards[--numberOfCards];
		this.cards[numberOfCards] =  null;
		return c;
	}
	
	public String toString(){
		String str = "";
			
		for(int i = 0; i < this.numberOfCards; i++){
			str += cards[i].toString() +"\n";
		}
			
		return str;
	}
	
	public void shuffle(){
		Card tempCard = null;
		int swapIndex = 0;
		for(int i = 0; i < numberOfCards; i++){
			swapIndex = this.rng.nextInt(numberOfCards);
			tempCard = this.cards[i];
			this.cards[i] = this.cards[swapIndex];
			this.cards[swapIndex] = tempCard;
		}
		
	}
	
}
