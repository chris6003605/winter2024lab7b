public enum Suit{
	
	HEARTS(0.4),
	SPADES(0.3),
	DIAMONDS(0.2),
	CLUBS(0.1);
	
	private final double scoreOfSuit;
	
	Suit(double scoreOfSuit){
		this.scoreOfSuit = scoreOfSuit;
	}
	public double getScoreOfSuit(){
		return this.scoreOfSuit;
	}
}