public class SimpleWar
{
	public static void main(String[] args){
		Deck deck = new Deck();
		int player1Points = 0;
		int player2Points = 0;
		double currentPlayer1Card = 0;
		double currentPlayer2Card = 0 ;
		
		deck.shuffle();
		
		
		for (int i =0; i < deck.length(); i++){
			Card player1Card = deck.drawTopCard();
		Card player2Card = deck.drawTopCard();
		
		System.out.print(player1Card + " ");
		currentPlayer1Card = player1Card.calculateScore();
		System.out.println(currentPlayer1Card);
		

		System.out.print(player2Card + " ");
		currentPlayer2Card = player2Card.calculateScore();
		System.out.println(currentPlayer2Card);
		System.out.println();
		
		if(currentPlayer1Card > currentPlayer2Card){
			
			player1Points += 1;
			System.out.println("Player 1 Wins! Current point for player 1: " + player1Points);
			System.out.println();
		}
		else{
			player2Points += 1;
			System.out.println("Player 2 Wins! Current point for player 2: " + player2Points);
			System.out.println();
		}
		}
		if(player1Points > player2Points){
			System.out.println("Player 1 Wins");
		}
		else{
			System.out.println("Player 2 Wins");
		}
		
	}
		
}
